import requests


class ProducerParser:
    link = ''
    filename = ''

    def __init__(self, link):
        self.link = link

    def _generate(self, content):
        return "test"

    def _save(self, html):
        file = open(self.filename + ".html", "w", encoding='utf-8')
        file.write(html)
        file.close()

    def load(self,):
        response = requests.get(self.link)
        html = self._generate(response.content)
        self._save(html)
