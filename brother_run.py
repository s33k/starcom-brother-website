from tkinter import *
from tkinter.ttk import Progressbar
from brother import Brother


def generate():
    progress['value'] = 0
    brother = Brother(link_field.get(), ad=ad_status)
    brother.load()
    progress['value'] = 100


window = Tk()
window.geometry('400x125')
window.title("Jebać drukarki (╯°□°）╯︵ ┻━┻  wersja BROTHER")

title_label = Label(window, text="Brother", font="Arial 10 bold")
title_label.grid(column=0, row=0, sticky=W)

link_label = Label(window, text="Link do strony drukarki")
link_label.grid(column=0, row=1, columnspan=2, sticky=W)

link_field = Entry(window, width=30)
link_field.grid(column=2, row=1, columnspan=2, sticky=W)

ad_status = BooleanVar
ad_check = Checkbutton(window, var=ad_status, text="promocja")
ad_check.grid(row=2, column=0, sticky=W)

btn = Button(window, text="Generuj stronę", command=generate, width=35)
btn.grid(column=0, row=6, columnspan=5, pady=5)

progress = Progressbar(window, length=400)
progress.grid(column=0, row=7, columnspan=4, sticky=W)

window.mainloop()