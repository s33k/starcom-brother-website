from producer_parser import ProducerParser
from bs4 import BeautifulSoup
from brother_html import *


class Brother(ProducerParser):

    ad = False

    def __init__(self, link, ad=False):
        super().__init__(link)
        self.ad = ad

    def _generate(self, content):
        soup = BeautifulSoup(content, 'html.parser')

        product_name = soup.find('h1', attrs={'itemprop': 'name'})
        self.filename = product_name.string

        key_features_parent = soup.find('section', attrs={'class': "product-detail--key-features"})
        key_features = key_features_parent.find_all('li')

        description_parent = soup.find(id='overview')
        description_div = description_parent.find(attrs={'class': 'col-md-8'})
        short_description = description_div.contents[0].__str__()
        description = ""
        for child in description_div.children:
            description += child.__str__()

        specifications = {}
        compatible_with = []
        main_specifications_parent = soup.find(id='specifications')
        if main_specifications_parent:
            specifications_sections = main_specifications_parent.find_all(attrs={'class': "common-accordion--item"})
            for specs in specifications_sections:
                specs_section_title = specs.find('h4').string
                trs = specs.find_all("tr")
                rows = {}
                for tr in trs:
                    th = tr.find('th').string
                    td = tr.find('td').string
                    rows[th] = td
                specifications[specs_section_title] = rows
        else:
            supplies_main_parent = soup.find(id='supplies')
            lis = supplies_main_parent.find_all('li')
            for li in lis:
                device_name = li.find('h4').string
                compatible_with.append(device_name)

        output = ''
        output += header
        if self.ad:
            output += ad
        output += title
        output = output.replace("{{name}}", product_name.string)

        output += description_html
        output = output.replace("{{desc}}", description)

        # for desc in description_div.contents:
        #     if desc != '\n' and desc.string is not None:
        #         output += description_html
        #         print(desc.string)
        #         output = output.replace("{{desc}}", desc.string)

        output += keys_title
        output += keys_header

        for key in key_features:
            output += keys_li
            output = output.replace('{{key-value}}', key.string)

        output += keys_footer

        if specifications != {}:
            output += specs_title
            for spec_key in specifications.keys():
                output += specs_header
                output = output.replace("{{specs_name}}", spec_key)
                for key in specifications[spec_key].keys():
                    output += specs_row
                    output = output.replace("{{specs_key}}", key)
                    output = output.replace("{{specs_value}}", specifications[spec_key][key])
                output += specs_footer
        else:
            output += comp_title
            output += comp_header
            for comp in compatible_with:
                output += comp_item
                output = output.replace('{{device_name}}', comp)
            output += comp_footer

        output += '</div>'
        return output

