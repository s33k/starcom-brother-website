header = '''
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.min.css" rel='stylesheet'>
<style>
.spacer{
	border-bottom: 1px solid #eee;
	margin: 20px 0;
}
.description{
	font-size: 1.25em;
	color: #666;
}
.keys{
	list-style: none;
	font-size: 1.5em;
}
.fa-check{
	color: #337ab7;
	font-size: 1.5em;
}
</style>
<div class="container-fluid">
'''

ad = '''
<div class="row">
		<div class="well well-lg">
			<h1 style="font-size: 2em;">PROMOCJA<small>1.11.2019 - 31.12.2019</small></h1>
			<p>
					Zasady: <br>
					Zarejestruj na stronie <a href="https://atyourside.brother.pl/register-your-device" target="_blank">https://atyourside.brother.pl/register-your-device</a> jedno z poniższych urządzeń Brother, i otrzymają 5 lat gwarancji ONSITE gratis!<br>
					5 lat gwarancji onsite obowiązuje wyłącznie na wybrane modele Brother objęte promocją i zarejestrowane na brother.pl w okresie promocyjnym.
			</p>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="list-group">
						<p class="list-group-item active">Laserowe monochromatyczne</p>
						<p class="list-group-item">HL-L5000D</p>
						<p class="list-group-item">HL-L5100DN</p>
						<p class="list-group-item">HL-L5200DW</p>
						<p class="list-group-item">HL-L6300DW</p>
						<p class="list-group-item">HL-L6400DW</p>
						<p class="list-group-item">DCP-L5500DN</p>
						<p class="list-group-item">DCP-L6600DW</p>
						<p class="list-group-item">MFC-L5700DN</p>
						<p class="list-group-item">MFC-L5750DW</p>
						<p class="list-group-item">MFC-L6800DW</p>
						<p class="list-group-item">MFC-L6900DW</p>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="list-group">
						<p class="list-group-item active">Laserowe kolorowe</p>
						<p class="list-group-item">HL-L8260CDW</p>
						<p class="list-group-item">HL-L8360CDW</p>
						<p class="list-group-item">HL-L9310CDW</p>
						<p class="list-group-item">DCP-L8410CDW</p>
						<p class="list-group-item">MFC-L8690CDW</p>
						<p class="list-group-item">MFC-L8900CDW</p>
						<p class="list-group-item">MFC-L9570CDW</p>
					</div>
				</div>
			</div>
			<p>
				Warunki promocji: <a href="https://www.brother.pl/5yw" target="_blank">https://www.brother.pl/5yw</a>
			</p>
		</div>
	</div>
'''

title = '''
<div class='page-header'>
    <h1 class="text-center" style="font-size: 3em;"><strong> {{name}} </strong></h1>
</div>
'''

description_html = '''
<div class="row">
	<div class="col-xs-12">
		<p class="text-center description">
			{{desc}} 
		</p>
	</div>
</div>
<br>
<br>
'''

keys_title = '''
<div class="row spacer"></div>
<div class="row">
	<h1 style="font-size: 2em" class="text-center">Kluczowe cechy</h1>
</div>
<br>
'''

keys_header = '''
<div class="row">
		<div class="col-xs-12 col-md-9 col-md-offset-3">
			<div class="center-block">
				<ul class="keys">
'''

keys_li = '''
					<li><i class="fas fa-check"></i> {{key-value}} </li>

'''

keys_footer = '''
				</ul>
			</div>
		</div>
	</div>
'''

comp_title = '''
	<div class="row spacer"></div>
	<div class="row">
		<h1 style="font-size: 2em" class="text-center">Kompatybilne z</h1>
	</div>
'''

comp_header = '''
<div class='row'>
		<div class="col-xs-12 col-md-4 col-md-offset-4 list-group">
'''

comp_item = '''
<p class='list-group-item text-center'> {{device_name}} </p>
'''

comp_footer = '''
		</div>
	</div>
'''

specs_title = '''
	<div class="row spacer"></div>
	<div class="row">
		<h1 style="font-size: 2em" class="text-center">Dane techniczne</h1>
	</div>
'''

specs_header = '''
	<div class="row">
		<div class="col-xs-12">
			<h2 style="font-size: 1.75em;"> {{specs_name}} </h2>
			<div class="table-responsive">
				<table class="table">
					<tbody>
'''

specs_row = '''
						<tr>
							<th><strong> {{specs_key}} </strong></th>
							<td> {{specs_value}} </td>
						</tr>
'''

specs_footer = '''
					</tbody>
				</table>
			</div>
		</div>
	</div>
'''